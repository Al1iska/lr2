﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР2_3_
{
    class Program
    {
        static void Main(string[] args)
        {
           
                Console.Write("Введите количество строк (N): ");
                int n = int.Parse(Console.ReadLine());

                Console.Write("Введите количество столбцов (M): ");
                int m = int.Parse(Console.ReadLine());

                if (n <= 0 || m <= 0)
                {
                    Console.WriteLine("Размерности матрицы должны быть положительными числами.");
                    return;
                }

                double[,] matrix = new double[n, m];

                // Ввод элементов матрицы
                Console.WriteLine("Введите элементы матрицы:");

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        Console.Write($"Элемент [{i}, {j}]: ");
                        matrix[i, j] = double.Parse(Console.ReadLine());
                    }
                }

                Console.WriteLine("Матрица:");

                // Вывод матрицы
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        Console.Write(matrix[i, j] + "\t");
                    }
                    Console.WriteLine();
                }

                int rank = ComputeMatrixRank(matrix, n, m);

            Console.WriteLine($"Ранг матрицы: {rank}");
            Console.ReadKey();
        }

            static int ComputeMatrixRank(double[,] matrix, int n, int m)
            {
                int rank = 0;
                double eps = 1e-9; // Погрешность для сравнения с нулем

                // Преобразование матрицы в верхнюю треугольную форму методом Гаусса-Жордана
                for (int row = 0; row < n; row++)
                {
                    int lead = row;
                    while (lead < m && Math.Abs(matrix[row, lead]) < eps)
                    {
                        lead++;
                    }

                    if (lead == m)
                        continue;

                    SwapRows(matrix, row, lead);

                    double leadValue = matrix[row, lead];
                    for (int col = 0; col < m; col++)
                    {
                        matrix[row, col] /= leadValue;
                    }

                    for (int otherRow = 0; otherRow < n; otherRow++)
                    {
                        if (otherRow != row)
                        {
                            double factor = matrix[otherRow, lead];
                            for (int col = 0; col < m; col++)
                            {
                                matrix[otherRow, col] -= factor * matrix[row, col];
                            }
                        }
                    }

                    rank++;
                }

                return rank;
            }

            static void SwapRows(double[,] matrix, int row1, int row2)
            {
                int cols = matrix.GetLength(1);
                for (int col = 0; col < cols; col++)
                {
                    double temp = matrix[row1, col];
                    matrix[row1, col] = matrix[row2, col];
                    matrix[row2, col] = temp;
                }
            }

    }
}

