﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР2_1_
{
    class Program
    {
            static void Main(string[] args)
            {
                Console.Write("Введите количество строк (N): ");
                int n = int.Parse(Console.ReadLine());

                Console.Write("Введите количество столбцов (M): ");
                int m = int.Parse(Console.ReadLine());

                if (n <= 0 || m <= 0)
                {
                    Console.WriteLine("Размерности матрицы должны быть положительными числами.");
                    return;
                }

                double[,] matrix = new double[n, m];

                // Ввод элементов матрицы
                Console.WriteLine("Введите элементы матрицы:");

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        Console.Write($"Элемент [{i}, {j}]: ");
                        matrix[i, j] = double.Parse(Console.ReadLine());
                    }
                }

                Console.WriteLine("Матрица:");

                // Вывод матрицы
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        Console.Write(matrix[i, j] + "\t");
                    }
                    Console.WriteLine();
                }

                // Вычисление среднего арифметического для строк
                Console.WriteLine("Средние арифметические для строк:");

                for (int i = 0; i < n; i++)
                {
                    double sum = 0;
                    for (int j = 0; j < m; j++)
                    {
                        sum += matrix[i, j];
                    }
                    double average = sum / m;
                    Console.WriteLine($"Среднее для строки {i}: {average}");
                }

                // Вычисление среднего арифметического для столбцов
                Console.WriteLine("Средние арифметические для столбцов:");

                for (int j = 0; j < m; j++)
                {
                    double sum = 0;
                    for (int i = 0; i < n; i++)
                    {
                        sum += matrix[i, j];
                    }
                    double average = sum / n;
                    Console.WriteLine($"Среднее для столбца {j}: {average}");
                }
            Console.ReadKey();
            }
    }


}
